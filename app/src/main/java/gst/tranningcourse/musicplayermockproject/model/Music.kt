package gst.tranningcourse.musicplayermockproject.model

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

/**
 * data class Music
 *
 *@param name: music name
 *@param artist: music artist
 *@param link: music link from storage
 *@param album: music album
 */
data class Music(
    val name: String?,
    val artist: String,
    val link: String,
    val album: String
) : Serializable
